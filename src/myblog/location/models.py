from django.db import models


# Create your models here.
class Province(models.Model):
    name = models.CharField(max_length=255, default=None, verbose_name='Tỉnh/thành')

    class Meta:
        db_table = 'd_location_province'
        verbose_name = 'Tỉnh/Thành'
        verbose_name_plural = 'Tỉnh/thành'

    def __str__(self):
        return self.name if self.name else '-'