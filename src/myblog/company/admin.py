from django.contrib import admin
from company.models import Company, Industry, Skill
# Register your models here.
admin.site.register(Company)
admin.site.register(Industry)
admin.site.register(Skill)
