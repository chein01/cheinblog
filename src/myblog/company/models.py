from django.db import models
from location.models import Province


# Create your models here.
class Industry(models.Model):
    name = models.CharField(max_length=255, default=None, verbose_name='Tên lĩnh vực')

    class Meta:
        db_table = 'd_company_industry'
        verbose_name = 'Lĩnh vực'
        verbose_name_plural = 'Lĩnh vực'

    def __str__(self):
        return self.name if self.name else '-'


class Skill(models.Model):
    name = models.CharField(max_length=255, default=None, verbose_name='Tên kĩ năng')
    industry = models.ForeignKey(
        Industry,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Lĩnh vực'
    )

    class Meta:
        db_table = 'd_company_skill'
        verbose_name = 'Kĩ năng'
        verbose_name_plural = 'Kĩ năng'

    def __str__(self):
        return self.name if self.name else '-'


class Company(models.Model):
    name = models.CharField(max_length=255, default=None, verbose_name='Tên công ty')
    industry = models.ForeignKey(
        Industry,
        on_delete=models.CASCADE,
        default=None,
        verbose_name='Lĩnh vực'
    )
    skill = models.ForeignKey(
        Skill,
        on_delete=models.CASCADE,
        default=None,
        verbose_name='Kỹ năng'
    )
    province = models.ForeignKey(
        Province,
        on_delete=models.CASCADE,
        default=None,
        verbose_name='Tỉnh/Thành'
    )

    class Meta:
        db_table = 'd_company'
        verbose_name = 'Công ty'
        verbose_name_plural = 'Công ty'

    def __str__(self):
        return self.name if self.name else '-'
