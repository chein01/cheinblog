from personal.models import Personal
from django.db.models import Q
from base.api.exceptions import NotFoundException
from django.contrib.auth.hashers import check_password
from base.validators import is_valid
import re

USER_INCORRECT_MESSAGE = 'The username or email is incorrect'
USER_NOT_EXIST_MESSAGE = 'The user does not exist'
PASS_WORD_INCORRECT_MESSAGE = 'The password is incorrect'
PASS_WORD_NOT_EXITS_MESSAGE = 'The password does not exist'
USER_EXISTS_MESSAGE = 'Account already exists'
USER_NAME_SPECIAL_CHARACTER = 'Username must not contain special characters'


def check_user_login(username, password):
    if username:
        user = Personal.objects.filter(Q(username=username) | Q(email=username)).first()
        if user is None:
            raise NotFoundException(message=USER_NOT_EXIST_MESSAGE)
        if not user.password:
            raise NotFoundException(message=PASS_WORD_NOT_EXITS_MESSAGE)
        if not check_password(password, user.password):
            raise NotFoundException(message=PASS_WORD_INCORRECT_MESSAGE)
        return user
    raise USER_INCORRECT_MESSAGE


def check_required_data(data):
    data_required = (
        ('username', None),
        ('password', None)
    )
    is_valid(data_required, data)


def isValidUsername(username):
    check_regex = re.compile('[^0-9a-zA-Z]+')
    return check_regex.search(username)


def check_dev_register(username, password):
    if isValidUsername(username):
        raise NotFoundException(message=USER_NAME_SPECIAL_CHARACTER)
    user = Personal.objects.filter(username=username).first()
    if user:
        raise NotFoundException(message=USER_EXISTS_MESSAGE)
    user = Personal(username=username, password=password)
    user.save()
    return user
