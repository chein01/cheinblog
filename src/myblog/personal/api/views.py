from base.api.responses import SuccessResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from personal.utils import check_user_login, check_required_data, check_dev_register
from personal.models import Personal
from django.db.models import Q
import datetime


# Create your views here.
class PersonalCreateView(APIView):

    def post(self, request, format=None):
        return SuccessResponse(message=None)


class DeveloperLogin(APIView):

    def post(self, request):
        data = request.data
        username = data.get('username')
        password = data.get('password')
        check_required_data(data)
        user = check_user_login(username, password)
        user.last_login = datetime.datetime.now()
        user.save()
        return Response({
            "results": {
                "code": 200,
                "message": "User existed",
                "new_user": False,
                "role": 'Developer' if user.company is None else user.company.name,
                "access_token": user.encode_auth_token(id=user.id),
                "fresh_token": user.create_refesh_token(id=user.id),
                'data': user.to_json()
            }
        })


class DeveloperRegister(APIView):

    def post(self, request):
        data = request.data
        username = data.get('username')
        password = data.get('password')
        check_required_data(data)
        user = check_dev_register(username, password)
        return Response({
            "results": {
                "code": 200,
                "message": "Register user successful",
                "new_user": True,
                "role": 'Developer' if user.company is None else user.company.name,
                "access_token": user.encode_auth_token(id=user.id),
                "fresh_token": user.create_refesh_token(id=user.id),
                'data': user.to_json()
            }
        })
