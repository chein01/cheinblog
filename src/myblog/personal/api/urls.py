from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url, include
from django.urls import path
from personal.api.views import DeveloperLogin, DeveloperRegister
urlpatterns = [
    path('developer/login', DeveloperLogin.as_view(), name='developer-login'),
    path('developer/register', DeveloperRegister.as_view(), name='developer-register'),
]


urlpatterns = format_suffix_patterns(urlpatterns)
