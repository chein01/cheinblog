from django.db import models
from personal.choices import *
import json
import jwt
import datetime
from django.conf import settings
from base.api.exceptions import PermissionDeniedException
from base.utils import format_datetime
from company.models import Company
# Create your models here.


class Personal(models.Model):
    email = models.CharField(max_length=128, null=True, blank=True, verbose_name="Email người dùng", unique=True)
    username = models.CharField(unique=True, max_length=255, verbose_name="Tên người dùng")
    password = models.CharField(max_length=255, null=True, blank=True, verbose_name="Mật khẩu")
    uid = models.CharField(max_length=500, null=True, blank=True, verbose_name="Social user id")
    provider = models.CharField(max_length=50, choices=PROVIDER, null=True, blank=True, verbose_name="Nguồn")
    created_at = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Ngày tạo")
    last_login = models.DateTimeField(null=True, blank=True, verbose_name="Lần đăng nhập cuối")
    company = models.ForeignKey(
        Company,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name='Công ty'
    )

    class Meta:
        db_table = 'd_personal'
        verbose_name = "Người sử dụng"
        verbose_name_plural = "Người sử dụng"
        indexes = [
            models.Index(fields=['uid', ]),
            models.Index(fields=['provider', ]),
        ]

    def encode_auth_token(self, id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=settings.JWT_DAYS_DELTA,
                                                                       seconds=settings.JWT_SECONDS_DELTA),
                'iat': datetime.datetime.utcnow(),
                'sub': id
            }
            return jwt.encode(
                payload=payload,
                key=settings.JWT_SECRET_KEY,
                algorithm=settings.JWT_ALGORITHM
            )
        except Exception as e:
            return e

    def create_refesh_token(self, id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=settings.JWT_DAYS_REFRESH_DELTA,
                                                                       seconds=settings.JWT_SECONDS_REFRESH_DELTA),
                'iat': datetime.datetime.utcnow(),
                'sub': id
            }
            return jwt.encode(
                payload=payload,
                key=settings.JWT_SECRET_KEY,
                algorithm=settings.JWT_ALGORITHM
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        try:
            payload = jwt.decode(auth_token, settings.JWT_SECRET_KEY)
            time_change = TimeChangePassword.objects.filter(personal_id=payload['sub']).first()
            if time_change and int(time_change.last_change) > payload['iat']:
                return TOKEN_EXPIRED
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return TOKEN_EXPIRED
        except jwt.InvalidTokenError:
            return TOKEN_INVALID

    @staticmethod
    def get_user_id(request):
        user_id = None
        try:
            access_token = request.META.get(HTTP_AUTHORIZATION)
            if access_token:
                access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]
                user_id = Personal.decode_auth_token(access_token)
        except:
            pass
        return user_id

    @staticmethod
    def get_or_error_user_id(request):
        try:
            access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]
            user_id = Personal.decode_auth_token(access_token)
        except:
            raise PermissionDeniedException
        return user_id

    @staticmethod
    def get_or_error_user_role(request):
        try:
            access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]
            app_type = request.data.get('app_type') if request.data else None
            user_id = Personal.decode_auth_token(access_token)
            if user_id:
                user = Personal.objects.select_related('organization', 'company').get(id=user_id)
                user_role = 'anonymous'
                if user.company:
                    user_role = user.company.type
                elif user.organization:
                    user_role = user.organization.screen

                if user_role == app_type:
                    return True
        except:
            raise PermissionDeniedException
        return False

    @staticmethod
    # get company_id, organization_id
    def get_or_error_user_object(request):
        try:
            access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]
            user_id = Personal.decode_auth_token(access_token)
            if user_id:
                user = Personal.objects.filter(id=user_id).first()
                if not user:
                    raise PermissionDeniedException
                return user
        except:
            raise PermissionDeniedException

    def to_json(self):
        return {
            "id": self.id,
            "user_name": self.username,
            "uid": self.uid,
            "provider": self.provider,
            "create_at": format_datetime(self.created_at),
            "last_login": format_datetime(self.last_login) if self.last_login else None,
        }

    # def get_extra_info(self):
    #     extra_info = self.extra_info if self.extra_info else None
    #     return json.loads(extra_info)

    def __str__(self):
        if self.username:
            return self.username
        return "-"


class ResetPasswordToken(models.Model):

    email = models.CharField(max_length=128, null=False, blank=False, verbose_name="Email người dùng", unique=True)
    token = models.PositiveIntegerField(null=False, blank=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'd_personal_reset_password'
        verbose_name = "Lấy lại mật khẩu"


class TimeChangePassword(models.Model):
    personal = models.ForeignKey(Personal, on_delete=models.CASCADE, null=True, blank=True)
    last_change = models.IntegerField(null=False, blank=False, verbose_name="Thời điểm đổi mật khẩu")

    class Meta:
        db_table = 'd_time_change_password'
        verbose_name = "Đổi mật khẩu lần cuối"