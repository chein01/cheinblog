from django.contrib import admin
from personal.models import Personal
from personal.forms import PersonalForm
from django.contrib.auth.hashers import make_password


class PersonalAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'email', 'company']
    list_display_links = ['id', 'username']
    form = PersonalForm

    def save_model(self, request, obj, form, change):
        old_objects = self.model.objects.filter(id=obj.id).first()
        if old_objects:
            if 'password' in form.changed_data:
                obj.password = make_password(obj.password)
            else:
                obj.password = old_objects.password
        else:
            obj.password = make_password(obj.password)

        super(PersonalAdmin, self).save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        base_form = super(PersonalAdmin, self).get_form(request, obj, **kwargs)

        class PersonalFormWithRequest(base_form):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                kwargs['obj'] = obj
                return base_form(*args, **kwargs)

        return PersonalFormWithRequest

# Register your models here.
admin.site.register(Personal, PersonalAdmin)
