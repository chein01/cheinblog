from django.apps import AppConfig


class PersonalConfig(AppConfig):
    name = 'personal'
    verbose_name = 'Người sử dụng'
