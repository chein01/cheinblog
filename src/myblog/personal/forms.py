from django import forms


class PersonalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.obj = kwargs.pop('obj', None)
        super(PersonalForm, self).__init__(*args, **kwargs)

        if self.obj:
            if self.obj.password:
                self.fields[
                    "password"].help_text = "Mật khẩu đã tồn tại nhưng không được hiển thị. Nhập mật khẩu mới nếu muốn thay đổi"
            self.initial['password'] = None
