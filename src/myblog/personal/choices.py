TOKEN_BLACKLIST = -1
TOKEN_EXPIRED = -2
TOKEN_INVALID = -3

FACEBOOK = 'facebook'
ZALO = 'zalo'
APPLE = 'apple'
GOOGLE = 'google'
GITHUB = 'github'

DEACTIVE = 0
ACTIVE = 1

MALE = "male"
FEMALE = "female"
PROVIDER = (
    (FACEBOOK, 'Facebook'),
    (GOOGLE, 'Google'),
    (GITHUB, 'Github')
)
ACTIVE_STATUS = (
    (DEACTIVE, 'Deactive'),
    (ACTIVE, 'Active'),
)
GENDER = (
    (FEMALE, FEMALE),
    (MALE, MALE)
)

HTTP_AUTHORIZATION = 'HTTP_AUTHORIZATION'