from .settings import MIDDLEWARE

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'db',
        'NAME': 'myblog',
        'USER': 'root',
        'PASSWORD': 'root',
        'OPTIONS': {
            'charset': 'utf8',
            # "init_command": "SET foreign_key_checks = 0;",
        }
    }
}
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'filters': {
#         'require_debug_true': {
#             '()': 'django.utils.log.RequireDebugTrue',
#         }
#     },
#     'handlers': {
#         'file': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': '/var/log/debug.log',
#         },
#         'console': {
#             'level': 'DEBUG',
#             'class': 'logging.StreamHandler',
#         },
#
#     },
#     'loggers': {
#         'django.db.backends': {
#             'level': 'DEBUG',
#             'handlers': ['console'],
#         },
#     },
# }
# MIDDLEWARE = MIDDLEWARE + ['base.middlewares.QueryCountDebugMiddleware']

