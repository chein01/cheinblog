from __future__ import unicode_literals

from rest_framework import status, exceptions
from rest_framework.response import Response
from base.api.responses import ErrorResponse


def my_error_handler(exc, *args):
    """
    Returns the response that should be used for any given exception.
    By default we handle the REST framework `APIException`, and some new exception we created...
    Any unhandled exceptions may return `None`, which will cause a 500 error
    to be raised.
    """
    if isinstance(exc, exceptions.APIException):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['X-Throttle-Wait-Seconds'] = '%d' % exc.wait

        return Response({'error': exc.detail},
                        status=exc.status_code,
                        headers=headers)

    elif isinstance(exc, TetNotFound):
        return Response({'error': {'code': 404, "message": TetNotFound.get_message(exc)}},
                        status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, TetPermissionDenied):
        return Response({'error': {'code': 403, "message": TetPermissionDenied.get_message(exc)}},
                        status=status.HTTP_403_FORBIDDEN)

    elif isinstance(exc, TetBadRequest):
        return Response({'error': {'code': 400, "message": TetBadRequest.get_message(exc)}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, TetBadFormatDate):
        return Response({'error': {'code': 400,
                                   "message": TetBadFormatDate.get_message(exc)}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, TetBadFormatNumber):
        return Response({'error': {'code': 400,
                                   "message": TetBadFormatNumber.get_message(exc)}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, TetBadImageFile):
        return Response({'error': {'code': 400,
                                   "message": TetBadImageFile.get_message(exc)}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, TetBadImageFormat):
        return Response({'error': {'code': 400,
                                   "message": TetBadImageFormat.get_message(exc)}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, TetMissingRequest):
        return Response({'error': {'code': 400, "message": TetMissingRequest.get_message(exc)}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, TetCreateFail):
        return Response({'error': {'code': 500, "message": TetCreateFail.get_message(exc)}},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif isinstance(exc, PersonalCreateUserExp):
        return Response({'error': {'code': 500, "message": "Cannot create user"}},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif isinstance(exc, PersonalUpdateUserExp):
        return Response({'error': {'code': 500, "message": "Cannot update user"}},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif isinstance(exc, PersonalDeviceTypeExp):
        return Response({'error': {'code': 400, "message": "invalid type"}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, PersonalBirthDayExp):
        return Response({"error": {"code": 400, "message": "birth_day missing request"}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, PersonalBirthDayFormatExp):
        return Response({"error": {"code": 400, "message": "Incorrect data format, should be YYYY-MM-DD"}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, PersonalDeviceExp):
        return Response({"error": {"code": 500, "message": "Cannot register this device"}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, YearFormatExp):
        return Response({"error": {"code": 400, "message": "Year must be integer and in range [1-20480"}},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, BookMarkContentTypeFormatExp):
        return Response(
            {"error": {"code": 400, "message": "content_type must be in 'article', 'culture', 'bookstory', 'hotdeal, 'video"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, BookMarkContentIdFormatExp):
        return Response(
            {"error": {"code": 400, "message": "content_type must be in 'article', 'culture', 'bookstory', 'hotdeal', 'video"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, LongitudeFormatExp):
        return Response(
            {"error": {"code": 400, "message": "longitude must be a number and in range [-180.0, 180.0]"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, LatitudeFormatExp):
        return Response(
            {"error": {"code": 400, "message": "latitude must be a number and in range [-90.0, 90.0]"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, LocationFormatExp):
        return Response(
            {"error": {"code": 400, "message": "latitude and longitude must be a number"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, LatitudeRequiredExp):
        return Response(
            {"error": {"code": 400, "message": "latitude is required"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, LongitudeRequiredExp):
        return Response(
            {"error": {"code": 400, "message": "longitude is required"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, JsonFormatExp):
        return Response(
            {"error": {"code": 400, "message": "extra_info must be a json string"}},
            status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, NotFoundException):
        return ErrorResponse(
            message=NotFoundException.get_message(exc),
            status=NotFoundException.get_status_code(exc)
        )
    elif isinstance(exc, BadRequestException):
        return ErrorResponse(
            message=BadRequestException.get_message(exc),
            status=BadRequestException.get_status_code(exc)
        )
    elif isinstance(exc, PermissionDeniedException):
        return ErrorResponse(
            message=PermissionDeniedException.get_message(exc),
            status=PermissionDeniedException.get_status_code(exc)
        )

    # Note: Unhandled exceptions will raise a 500 error.
    return None


class TetException(Exception):
    message = 'Exception'

    def __init__(self, message=None):
        if message:
            self.message = message

    def get_message(self):
        return self.message


class TetNotFound(TetException):
    message = 'Không có nội dung tương ứng'


class TetPermissionDenied(TetException):
    message = 'Permission Denied'


class TetBadRequest(TetException):
    message = 'Bad Request'


class TetBadFormatDate(TetException):
    message = 'Incorrect format parameter: Wrong date format, should be YYYY-MM-DD'


class TetMissingRequest(TetException):
    message = 'Missing Request'


class TetCreateFail(TetException):
    message = 'Create Failed'


class TetBadImageFile(TetException):
    message = 'Wrong image file'


class TetBadImageFormat(TetException):
    message = 'Wrong image format, should be jpg, png or jpeg'


class TetBadFormatNumber(TetException):
    message = 'Incorrect format parameter: Wrong number format'


class PersonalCreateUserExp(Exception):
    pass


class PersonalUpdateUserExp(Exception):
    pass


class PersonalDeviceTypeExp(Exception):
    pass


class PersonalDeviceExp(Exception):
    pass


class PersonalBirthDayExp(Exception):
    pass


class PersonalBirthDayFormatExp(Exception):
    pass


class YearFormatExp(Exception):
    pass


class BookMarkContentTypeFormatExp(Exception):
    pass


class BookMarkContentIdFormatExp(Exception):
    pass


class LongitudeFormatExp(Exception):
    pass


class LatitudeFormatExp(Exception):
    pass


class LongitudeRequiredExp(Exception):
    pass


class LatitudeRequiredExp(Exception):
    pass


class LocationFormatExp(Exception):
    pass


class JsonFormatExp(Exception):
    pass


class CustomAPIException(Exception):

    def __init__(self, message='Exception', status_code=status.HTTP_400_BAD_REQUEST):
        if message:
            self.message = message
        if status_code:
            self.status_code = status_code

    def get_message(self):
        return self.message

    def get_status_code(self):
        return self.status_code


class NotFoundException(CustomAPIException):
    message = 'Not found data.'
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, message=message, status_code=status_code):
        super(NotFoundException, self).__init__(message=message, status_code=status_code)


class BadRequestException(CustomAPIException):
    message = 'Bad Request.'
    status_code = status.HTTP_400_BAD_REQUEST

    def __init__(self, message=message, status_code=status_code):
        super(BadRequestException, self).__init__(message=message, status_code=status_code)


class PermissionDeniedException(CustomAPIException):
    message = 'Permission Denied'
    status_code = status.HTTP_403_FORBIDDEN

    def __init__(self, message=message, status_code=status_code):
        super(PermissionDeniedException, self).__init__(message=message, status_code=status_code)
