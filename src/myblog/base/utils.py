from bs4 import BeautifulSoup


def clean_html(html, length=False):
    """
    clean html code, return text
    1. remove all script and style elements
    2. break into lines and remove leading and trailing space on each
    3. break multi-headlines into a line each
    4. drop blank lines
    """
    if not html:
        return None
    html = html.replace('><', '> <')
    soup = BeautifulSoup(html, 'html.parser')
    for script in soup(['script', 'style']):
        script.decompose()

    text = soup.get_text()
    lines = (line.strip() for line in text.splitlines())
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    text = '\n'.join(chunk for chunk in chunks if chunk)

    # return cleaned text
    return text[:length] if text and length else text


def format_datetime(datetime):
    return datetime.strftime("%d/%m/%Y %H:%M:%S") if datetime else None