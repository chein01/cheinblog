from django.db import models


# Create your models here.

class BaseDateTime(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Ngày tạo')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Ngày cập nhật')
    published_at = models.DateTimeField(null=True, blank=True, verbose_name='Ngày phát hành')

    class Meta:
        abstract = True
