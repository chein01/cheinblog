from django.db import models
from ckeditor.fields import RichTextField
from post.choices import POST_STATUS, DRAFT_KEY
from django.utils.text import slugify
from django.contrib.auth.models import User
from base.models import BaseDateTime


# Create your models here.
class Tag(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nhãn')
    slug = models.SlugField(max_length=255, unique=True, verbose_name='Slug', blank=True)

    class Meta:
        verbose_name = 'Nhãn bài viết'
        verbose_name_plural = 'Nhãn bài viết'
        db_table = 'd_tag'

    def save(self, *args, **kwargs):
        if self.name and not self.slug:
            self.slug = slugify(self.name)
        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.name if self.name else '-'


class Post(BaseDateTime):
    title = models.CharField(max_length=255, verbose_name='Tiêu đề')
    content = RichTextField(verbose_name='Nội dung')
    status = models.CharField(max_length=50, choices=POST_STATUS, default=DRAFT_KEY, verbose_name='Trạng thái')
    slug = models.SlugField(max_length=255, unique=True, verbose_name='Slug', blank=True)
    tag = models.ManyToManyField(Tag, blank=True, verbose_name='Nhãn bài viết')
    user = models.ForeignKey(User, verbose_name='Người dùng', on_delete=models.PROTECT, default=1)

    class Meta:
        verbose_name = 'Bài viết'
        verbose_name_plural = 'Bài viết'
        db_table = 'd_post'

    def save(self, *args, **kwargs):
        if self.title and not self.slug:
            self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return self.title if self.title else '-'
