from rest_framework.urlpatterns import format_suffix_patterns
from .views import PostList, PostDetail
from django.conf.urls import url
from django.urls import path

urlpatterns = [
    path('', PostList.as_view(), name='list-post'),
    path('detail/<int:pk>', PostDetail.as_view(), name='detail-post'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
