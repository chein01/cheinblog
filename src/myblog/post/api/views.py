from rest_framework import generics
from post.models import Post
from post.api.serializers import PostListSerializer, PostDetailSerializer
from base.api.exceptions import TetNotFound
from base.api.responses import SuccessResponse, ErrorResponse


class PostList(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostListSerializer

    def get_queryset(self):
        queryset = self.queryset
        status = self.request.GET.get('status')
        if status:
            queryset = queryset.filter(status=status)
        return queryset


class PostDetail(generics.RetrieveAPIView):
    serializer_class = PostDetailSerializer

    def get_object(self):
        try:
            return Post.objects.get(id=self.kwargs['pk'])
        except:
            raise TetNotFound

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = PostDetailSerializer(instance)
        return SuccessResponse(data=serializer.data)

    def put(self, request, *args, **kwargs):
        error_messages = {}
        instance = self.get_object()
        data = request.data
        serializer = PostDetailSerializer(instance, data=data)
        if not serializer.is_valid():
            error_messages.update(serializer.error_messages)
            return ErrorResponse(message=error_messages)
        serializer.save()
        return SuccessResponse(data=data)