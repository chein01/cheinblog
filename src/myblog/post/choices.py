# Post status
PUBLISHED_KEY = 'published'
PUBLISHED = 'Phát hành'
DRAFT_KEY = 'draft'
DRAFT = 'Nháp'
POST_STATUS = (
    (PUBLISHED_KEY, PUBLISHED),
    (DRAFT_KEY, DRAFT)
)