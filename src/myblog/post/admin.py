from django.contrib import admin
from post.models import *
from base.utils import clean_html


class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'content_brief', 'status', 'created_at', 'updated_at']
    search_fields = ['title']

    def content_brief(self, obj):
        return clean_html(obj.content[:200]) if obj.content else None


# Register your models here.
admin.site.register(Tag)
admin.site.register(Post, PostAdmin)
